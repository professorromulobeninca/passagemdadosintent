package br.edu.ifsc.modelolistviews.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.ifsc.modelolistviews.R;
import br.edu.ifsc.modelolistviews.models.Fruta;
import br.edu.ifsc.modelolistviews.controler.Frutas;

public class ApresentaInformacoesFruta extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apresenta_informacoes_fruta);
        //Recuperando dados transmitidos na intent
        Intent intent=getIntent();
        int position=intent.getExtras().getInt("id_item");
        //Recarregando Objetos
        Frutas frutas= new Frutas();
        final ArrayList<Fruta> listaFrutas = new ArrayList<>();
        for (Fruta f: frutas.FRUTAS) {
            listaFrutas.add(f);
        }

        ImageView imageView = findViewById(R.id.foto);
        TextView textView= findViewById(R.id.nome);

        Fruta f = listaFrutas.get(position);
        textView.setText(f.getNome());
        imageView.setImageResource(f.getImagem());

    }
}
