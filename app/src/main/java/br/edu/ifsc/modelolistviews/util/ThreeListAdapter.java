package br.edu.ifsc.modelolistviews.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.ifsc.modelolistviews.R;
import br.edu.ifsc.modelolistviews.models.Fruta;


public class ThreeListAdapter extends ArrayAdapter<Fruta> {
    private static  final String TAG = "ThreeListAdapter";

    private Context mContext;
    private  int mResource;

    /**
     * Default Construtor
     * @param context
     * @param resource
     * @param objects
     */
    public ThreeListAdapter( Context context, int resource, ArrayList<Fruta> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource= resource;

    }


    @Override
    public View getView(int position, View convertView,  ViewGroup parent) {
        //Adquirindo dados de um determinado objeto na posição do arrayList infromado  para carregar o ListView
        int codigo =getItem(position).getCodigo();
        String nome = getItem(position).getNome();
        String descricao = getItem(position).getDescricao();

        //


        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView= inflater.inflate(mResource, parent, false);

        TextView tvCodigo = (TextView) convertView.findViewById(R.id.text1);
        TextView tvNome = (TextView) convertView.findViewById(R.id.text2);
        TextView tvDescricao = (TextView) convertView.findViewById(R.id.text3);

        tvCodigo.setText(""+codigo);
        tvNome.setText( nome);
        tvDescricao.setText(descricao);

        return convertView;




    }
}
